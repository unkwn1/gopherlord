package gopherlord

import (
	"encoding/json"
	"fmt"
	"os"
	"sync"

	"github.com/melbahja/goph"
	"github.com/sirupsen/logrus"
)

// TODO: add errors to every function return for testing

// HostConfigs - contains a slice of Host pointers
type HostConfigs struct {
	Hosts []*Host
}

// Host - contains settings an SSH host for connection.
type Host struct {
	Hostname string
	Ip       string
	User     string
	Port     int
	Key      string
}

// Client - an abstract of goph.Client. It is used to create and populate the connect channel
type Client *goph.Client

/*
ReadConf - takes a string filepath to a JSON config file. Unmarsal returning a HostConfigs object.

It does not check for file existing nor set a default file. Thats done in the CLI rootCmd
*/
func ReadConf(c string, log *logrus.Logger) (HostConfigs, error) {
	var configs HostConfigs
	file, err := os.ReadFile(c)
	if err != nil {
		log.WithFields(logrus.Fields{
			"config file": c,
		}).Error("error reading config file")
		return configs, err
	}
	jerr := json.Unmarshal(file, &configs.Hosts)
	if jerr != nil {
		log.WithFields(logrus.Fields{
			"config file": c,
		}).Error("error unmarshalling config file")
		return configs, err
	}
	return configs, nil
}

/*
Connect - a method bound to a created Host. Takes a channel and waitgroup as it's arguments.

If successful connect to the SSH host pass Client to the channel.

TODO: needs to handle errors. Err channel or just stderrr?
*/
func (host *Host) Connect(ch chan Client, wg *sync.WaitGroup, log *logrus.Logger) {
	defer wg.Done()
	cLog := log.WithFields(logrus.Fields{
		"ipaddr": host.Ip,
		"user":   host.User,
		"prvKey": host.Key,
	})
	auth, aerr := goph.Key(host.Key, "")
	if aerr != nil {
		cLog.Error("failed to generate SSH auth agent")
		return
	}
	/*
		TODO: check / build client with goph.NewConn
		if host.Port != 22 {
		}
	*/
	client, cerr := goph.New(host.User, host.Ip, auth)
	if cerr != nil {
		cLog.Error("failed to establish connection to SSH host")
		return
	}
	cLog.Info("Connection established!")
	ch <- client
}

func RunCmd(c *goph.Client, in string, log *logrus.Logger) ([]byte, error) {
	out, err := c.Run(in)
	defer c.Close()
	if err != nil {
		log.WithFields(logrus.Fields{
			"host": c.Config.Addr,
			"cmd":  in,
		})
		return nil, err
	}
	fmt.Println(fmt.Sprintln(string(out)))
	return out, nil
}
