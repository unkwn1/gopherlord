/*
Copyright © 2022 unkwn1

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
package cmd

import (
	"fmt"
	"os"
	"sync"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/unkwn1/gopherlord/pkg/gopherlord"
)

// either a supplied config filepath or populated with defaultCfg
var cfgFile string

var defaultCfg string

var log = logrus.New()

//var inputCmd string

var wg sync.WaitGroup

/*
rootCmd contains gopherlord configuration and run logic.

Requires a minimum of one argument.Multiple args are combined into one command.

Example:

`gopherlord "hostname" "-s"`
  - would run the command `hostname -s` on the host.
*/
var rootCmd = &cobra.Command{
	Use:     "gopherlord <command>",
	Short:   "Send a command to multiple SSH hosts.",
	Long:    `Gopherlord is a simple CLI used to send a single command to many SSH clients`,
	Example: "gopherlord --config <fp> ss -tulpn",
	Args:    cobra.MinimumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		// if no config filepath supplied, use defaultCfg
		if cfgFile == "" {
			cfgDir, _ := os.UserConfigDir()
			defaultCfg := cfgDir + "/gopherlord.json"
			cfgFile = defaultCfg
			log.Warnf("No config path supplied. Using default %s", cfgFile)
		}
		fmt.Println("args supplied: ", args)
		cfgs, cfgerr := gopherlord.ReadConf(cfgFile, log)
		if cfgerr != nil {
			log.WithFields(logrus.Fields{
				"config file": cfgFile,
				"msg":         cfgerr,
			})
			os.Exit(1)
		}
		log.WithFields(logrus.Fields{
			"config_file": cfgFile,
		}).Info("INFO: Loaded config file")
		ch := make(chan gopherlord.Client, len(cfgs.Hosts))
		for _, host := range cfgs.Hosts {
			wg.Add(1)
			go host.Connect(ch, &wg, log)
		}
		wg.Wait()
		close(ch)
		// TODO: error array to log on finish
		for v := range ch {
			out, err := gopherlord.RunCmd(v, args[0], log)
			if err != nil {
				continue
			}

			//v.Client.Close()
			log.WithFields(logrus.Fields{
				"host":     v.Config.Addr,
				"command":  args[0],
				"response": string(out[:]),
			}).Info("Closed SSH connection.")
		}
		log.Info("gopherlord finished running.")
	},
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		log.Errorf("%s", err)
		os.Exit(1)
	}
}

// Aadd flags and builds the logger for the program
func init() {
	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", defaultCfg, "config file (default is $PWD/hosts.json)")
	log.SetFormatter(&logrus.JSONFormatter{})
	log.Out = os.Stdout
	log.SetLevel(logrus.WarnLevel)
	rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
